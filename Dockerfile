FROM node:18.12.1-alpine as builder

# Create a place in the container to process the node-js application in
RUN mkdir -p /usr/src/node-js-template
WORKDIR /usr/src/node-js-template

# Add package file
COPY package*.json ./
COPY tsconfig.json ./

# Build the node-js application, including devDependencies and create a production build
RUN npm install -g npm@9.3.0
RUN npm install
RUN npm install typescript -g

# Copy source
COPY . .

# Build dist
RUN tsc

# check files list
RUN ls -all

FROM node:18.12.1-alpine as runner

# Create a separate folder for the application to live in
WORKDIR /usr/src/node-js-template

# Copy the NPM modules
COPY --from=builder /usr/src/node-js-template/node_modules ./node_modules

# Copy other files to build directory
COPY --from=builder /usr/src/node-js-template/dist ./dist
COPY --from=builder /usr/src/node-js-template/.env ./dist/.env
COPY --from=builder /usr/src/node-js-template/.env.prod ./dist/.env.prod

# Copy static and env files
# COPY /usr/src/node-js-template dist/src/public

# Expose port 5000
EXPOSE 5000

CMD [ "node","./dist/src/index.js" ]
