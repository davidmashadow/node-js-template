import express from 'express';
import helmet from 'helmet';
import cors from 'cors';
import fileUpload from 'express-fileupload';
import path from 'path';
import swaggerUi from 'swagger-ui-express';
import * as swaggerDocument from '../swagger.json';
import APIERROR from './responses/apierror';
import { config } from './config';
import { initCrons } from './scripts/utils/scheduler';
import { crones } from './scripts/handlers';
import { loaders } from './loaders';
import { mailer } from './scripts/events';
import { errorMiddleware } from './middlewares/errorhandler';
import {
  AuthRoute,
  UserRoute,
  CardRoute,
  CurrencyRoute,
  PlanRoute,
  TaskRoute,
} from './api-routes';

config();
loaders();
mailer();
initCrons(crones);

const app = express();
app.use(cors());
app.use('/uploads', express.static(path.join(__dirname, './', 'uploads')));
app.use(express.json());
app.use(
  helmet({
    contentSecurityPolicy: {
      useDefaults: true,
      directives: {
        'connect-src': [
          "'self'",
          `${process.env.APP_PROTOCOL}://${process.env.APP_URL}`,
        ],
      },
    },
  })
);
app.use(fileUpload());

app.listen(process.env.APP_PORT, () => {
  app.get('/', (req, res) => {
    res.send('Backend is ready to serve');
  });
  app.use('/api-doc', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
  app.use('/api', AuthRoute);
  app.use('/api', UserRoute);
  app.use('/api', CardRoute);
  app.use('/api', CurrencyRoute);
  app.use('/api', PlanRoute);
  app.use('/api', TaskRoute);

  // !Error Handler
  app.use(
    (
      req: express.Request,
      res: express.Response,
      next: express.NextFunction
    ) => {
      next(new APIERROR('API Request not found', 404));
    }
  );

  app.use(errorMiddleware);
});
