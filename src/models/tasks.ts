import Mongoose from 'mongoose';
import logger from '../scripts/logger/tasks';

const TASKSCHEMA = new Mongoose.Schema(
  {
    title: String,
    description: String,
    assignedTo: {
      type: Mongoose.Types.ObjectId,
      ref: 'User',
    },
    dueDate: Date,
    statuses: [String],
    userId: {
      type: Mongoose.Types.ObjectId,
      ref: 'User',
    },
    projectId: {
      type: Mongoose.Types.ObjectId,
      ref: 'Project',
    },
    sectionId: {
      type: Mongoose.Types.ObjectId,
      ref: 'Section',
    },
    order: Number,
    isCompleted: Boolean,
    comments: [
      {
        comment: String,
        createdAt: Date,
        userId: {
          type: Mongoose.Types.ObjectId,
          ref: 'User',
        },
      },
    ],
    media: [
      {
        file: String,
        userId: {
          type: Mongoose.Types.ObjectId,
          ref: 'User',
        },
      },
    ],
    subTasks: [
      {
        type: Mongoose.Types.ObjectId,
        ref: 'Task',
      },
    ],
  },
  { timestamps: true, versionKey: false }
);

// Loglama
TASKSCHEMA.post('save', (doc: any) => {
  logger.log({ level: 'info', message: doc });
});

export const TASKRULE = Mongoose.model('Task', TASKSCHEMA);
