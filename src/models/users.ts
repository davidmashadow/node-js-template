import mongoose from 'mongoose';

const USERSCHEMA = new mongoose.Schema(
  {
    name: { type: String, required: true },
    email: { type: String, required: true },
    phoneNumber: { type: String, required: true },
    password: { type: String, required: true },
    profileImage: { type: String, required: false },
    role: { type: Array, required: false, default: ['ROLE_USER'] },
    isActivated: { type: Boolean, required: false, default: false },
    activationCode: { type: String, required: false, default: null },
  },
  { timestamps: true, versionKey: false }
);

export const USERRULE = mongoose.model('User', USERSCHEMA);
