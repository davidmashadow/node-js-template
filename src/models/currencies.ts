import Mongoose from 'mongoose';
// import logger from '../scripts/logger/currencies';

const CURRENCYSCHEMA = new Mongoose.Schema(
  {
    unit: { type: String, required: true },
    buy: { type: String, required: true },
    sell: { type: String, required: true },
    changeRate: { type: String, required: true },
    updateTime: { type: String, required: true },
  },
  { timestamps: false, versionKey: false }
);

/*
CURRENCYSCHEMA.post('save', (doc: any) => {
  logger.log({ level: 'info', message: doc });
});
*/

export const CURRENCYRULE = Mongoose.model('Currency', CURRENCYSCHEMA);
