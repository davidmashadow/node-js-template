import Mongoose from 'mongoose';
import logger from '../scripts/logger/plans';

const PLANSCHEMA = new Mongoose.Schema(
  {
    planId: {
      type: Mongoose.Types.ObjectId,
      ref: 'Plan',
      required: false,
    },
    code: String,
    name: String,
    description: String,
    price: String,
    dayRange: String,
  },
  { timestamps: true, versionKey: false }
);

// Loglama
PLANSCHEMA.post('save', (doc: any) => {
  logger.log({ level: 'info', message: doc });
});

export const PLANRULE = Mongoose.model('Plan', PLANSCHEMA);
