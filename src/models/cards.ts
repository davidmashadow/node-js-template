import Mongoose from 'mongoose';
import logger from '../scripts/logger/cards';

const CARDSCHEMA = new Mongoose.Schema(
  {
    userId: { type: Mongoose.Types.ObjectId, ref: 'User', required: true },
    cardId: {
      type: Mongoose.Types.ObjectId,
      ref: 'Card',
      required: false,
    },
    billing: {
      address: String,
      city: String,
      contactName: String,
      country: String,
      zipCode: String,
    },
    cardAlias: String,
    cardHolderName: String,
    cardNumber: Number,
    cvc: Number,
    expireMonth: Number,
    expireYear: Number,
    lastFourDigits: Number,
  },
  { timestamps: true, versionKey: false }
);

// LOGGER
CARDSCHEMA.post('save', (doc: any) => {
  logger.log({ level: 'info', message: doc });
});

export const CARDRULE = Mongoose.model('Card', CARDSCHEMA);
