import { USERRULE } from '../../models/users';
import { logger } from '../logger/general';

module.exports = async () => {
  try {
    logger().debug(`userlist::CRONEJOB.updateMany()`);
    await USERRULE.updateMany({}, { activationCode: null }).then((response) =>
      logger().info(
        `Success : ${response?.acknowledged} | Crone job modified ${response?.modifiedCount} users.`
      )
    );
  } catch (error) {
    logger().error(`${error}::USERSERVICE.updateMany()`);
  }
};
