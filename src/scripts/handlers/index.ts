// Every day at 03:00 am | 0 3 * * * *

export const crones = {
  resetActivationCode: {
    frequency: '0 3 * * *',
    handler: 'handlers/resetactivations',
  },
};
