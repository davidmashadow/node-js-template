import nodemailer from 'nodemailer';
import { eventEmitter } from './eventemitter';

export const mailer = () => {
  eventEmitter.on('sendEmail', (emailData: any) => {
    const transporter = nodemailer.createTransport({
      host: `${process.env.EMAIL_HOST}`,
      port: 587,
      auth: {
        user: `${process.env.EMAIL_USER}`,
        pass: `${process.env.EMAIL_PASSWORD}`,
      },
    });

    const info = transporter.sendMail({
      from: `${process.env.EMAIL_FROM}`,
      ...emailData,
    });
  });
};
