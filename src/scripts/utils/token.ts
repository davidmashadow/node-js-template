import JWT from 'jsonwebtoken';
import { UserType } from '../../types/users';

const generateAccessToken = (user: UserType): string =>
  JWT.sign(
    { name: user?.name, ...user },
    `${process.env.ACCESS_TOKEN_SECRET_KEY}`,
    {
      expiresIn: '1w',
    }
  );

const generateRefreshToken = (user: UserType): string =>
  JWT.sign(
    { name: user?.name, ...user },
    `${process.env.REFRESH_TOKEN_SECRET_KEY}`,
    {
      expiresIn: '1w',
    }
  );

export { generateAccessToken, generateRefreshToken };
