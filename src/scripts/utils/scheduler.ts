import cron from 'node-cron'; // https://crontab.guru/
import path from 'path';

const initCrons = (config: any) => {
  const activePath = path.join(__dirname, '..');

  Object.keys(config).forEach((key) => {
    if (cron.validate(config[key].frequency)) {
      cron.schedule(`${config[key].frequency}`, () => {
        // eslint-disable-next-line @typescript-eslint/no-var-requires,global-require,import/no-dynamic-require
        const handler = require(`${activePath}/${config[key].handler}`);
        handler();
      });
    }
  });
};

export { initCrons };
