import CryptoJS from 'crypto-js';

const passwordToHash = (password: string): string => {
  const hashAlgorithm = CryptoJS.HmacSHA1(
    password,
    `${process.env.PASSWORD_HASH}`
  ).toString();
  return CryptoJS.HmacSHA256(password, hashAlgorithm).toString();
};

export { passwordToHash };
