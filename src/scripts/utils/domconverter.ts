export function outerHTML(node: any): string {
  return node.outerHTML || new XMLSerializer().serializeToString(node);
}
