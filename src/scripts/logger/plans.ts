import winston from 'winston';

const logger = winston.createLogger({
  level: 'info',
  format: winston.format.json(),
  defaultMeta: { service: 'plan-service' },
  transports: [
    //
    // - Write all logs with importance level of `error` or less to `error.log`
    // - Write all logs with importance level of `info` or less to `combined.log`
    //
    new winston.transports.File({
      filename: 'src/logs/plans/error.log',
      level: 'error',
    }),
    new winston.transports.File({
      filename: 'src/logs/plans/info.log',
      level: 'info',
    }),
    new winston.transports.File({ filename: 'src/logs/plans/combined.log' }),
    new winston.transports.Console(),
  ],
});

export = logger;
