import winston from 'winston';

const { printf, combine, errors, json, colorize } = winston.format;

export const logger = () =>
  winston.createLogger({
    level: 'debug',
    format: combine(
      winston.format((info) => {
        // eslint-disable-next-line no-param-reassign
        info.level = info.level.toUpperCase();
        return info;
      })(),
      colorize(),
      winston.format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss' }),
      errors({ stack: true }),
      // json()
      printf(
        (info) =>
          `${info.timestamp} [${info.level}] : ${JSON.stringify(
            info.message,
            undefined,
            2
          )}`
      )
    ),
    defaultMeta: { service: 'user-service' },
    transports: [new winston.transports.Console()],
  });
