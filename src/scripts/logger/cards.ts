import winston from 'winston';

const logger = winston.createLogger({
  level: 'info',
  format: winston.format.json(),
  defaultMeta: { service: 'card-service' },
  transports: [
    //
    // - Write all logs with importance level of `error` or less to `error.log`
    // - Write all logs with importance level of `info` or less to `combined.log`
    //
    new winston.transports.File({
      filename: 'src/logs/cards/error.log',
      level: 'error',
    }),
    new winston.transports.File({
      filename: 'src/logs/cards/info.log',
      level: 'info',
    }),
    new winston.transports.File({ filename: 'src/logs/cards/combined.log' }),
    new winston.transports.Console(),
  ],
});

export = logger;
