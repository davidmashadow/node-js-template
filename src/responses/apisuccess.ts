interface ApiSuccessType {
  success: {
    message: string;
    data: any;
  };
}

const APISUCCESS = (message: string, data: any): ApiSuccessType => ({
  success: { message, data },
});

export { APISUCCESS };
