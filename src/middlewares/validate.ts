import { Request, Response, NextFunction } from 'express';
import httpStatus from 'http-status';
import { ObjectSchema, ValidationErrorItem } from 'joi';
import APIERROR from '../responses/apierror';
import { logger } from '../scripts/logger/general';

const validate =
  (context: ObjectSchema) =>
  (req: Request, res: Response, next: NextFunction): void => {
    const { value, error } = context.validate(req?.body);

    if (error) {
      const errorMessage = error?.details
        ?.map((detail: ValidationErrorItem): string => detail.message)
        .join();
      logger().error(errorMessage);
      next(new APIERROR(`${errorMessage}`, httpStatus.BAD_REQUEST));
    }

    Object.assign(req, value);
    next();
  };

export { validate };
