import { Request, Response, NextFunction } from 'express';

const errorMiddleware = (
  err: any,
  req: Request,
  res: Response,
  next: NextFunction
) => {
  res.status(err.status || 500);
  res.json({
    error: {
      message: err.message || 'Internal Server Error...',
    },
  });
};

export { errorMiddleware };
