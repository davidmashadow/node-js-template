import { Request, Response, NextFunction } from 'express';
import httpStatus from 'http-status';
import APIERROR from '../responses/apierror';

const idChecker =
  (field: string) => (req: Request, res: Response, next: NextFunction) => {
    if (!req?.params[field]?.match(/^[0-9a-fA-F]{24}$/)) {
      next(new APIERROR('Please enter valid ID value', httpStatus.BAD_REQUEST));
    }
    return next();
  };

export { idChecker };
