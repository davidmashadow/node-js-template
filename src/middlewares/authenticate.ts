import httpStatus from 'http-status';
import JWT from 'jsonwebtoken';
import { RequestHandler } from 'express';

const authenticate: RequestHandler = (req, res, next): any => {
  const token = req.headers?.authorization?.split(' ')[1] || null;

  if (token === null)
    return res.status(httpStatus.UNAUTHORIZED).send({
      error: 'If you continue this operation. You must log in first',
    });

  JWT.verify(token, `${process.env.ACCESS_TOKEN_SECRET_KEY}`, (err, user) => {
    if (err) return res.status(httpStatus.FORBIDDEN).send({ error: err });

    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    req.user = user;
    return next();
  });
  return null;
};

export { authenticate };
