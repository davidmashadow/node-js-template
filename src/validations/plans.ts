import Joi from 'joi';

class PLANVALIDATION {
  list = Joi.object({
    planId: Joi.string().required().min(24).max(24),
  });

  create = Joi.object({
    code: Joi.string()
      .required()
      .regex(/^[A-Z]*$/)
      .min(3)
      .max(30),
    name: Joi.string().min(2).max(30).required(),
    description: Joi.string().min(2).max(50),
    price: Joi.string().min(1).max(5).required(),
    dayRange: Joi.string().min(1).max(5).required(),
    role: Joi.array().required().items(Joi.string().valid('ROLE_ADMIN')),
  });
}

export = new PLANVALIDATION();
