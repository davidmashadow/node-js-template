import Joi from 'joi';

class USERVALIDATION {
  create = Joi.object({
    name: Joi.string().alphanum().min(3).max(30).required(),
    email: Joi.string().email().min(3).max(50).required(),
    phoneNumber: Joi.string().min(10).max(30).required(),
    password: Joi.string().min(6).max(30).required(),
    role: Joi.array().items(Joi.string().valid('ROLE_USER')),
    isActivated: Joi.boolean(),
  });

  activate = Joi.object({
    key: Joi.string().required().min(31).max(33),
  });

  update = Joi.object({
    name: Joi.string().min(3),
    email: Joi.string().email().min(3),
    password: Joi.string().required().min(6),
  });

  login = Joi.object({
    password: Joi.string().required().min(6),
    email: Joi.string().email().required().min(3),
  });

  passwordReset = Joi.object({
    email: Joi.string().email().required().min(3),
  });

  passwordResetComplete = Joi.object({
    email: Joi.string().email().required().min(3),
    key: Joi.string().required().min(31).max(33),
    newPassword: Joi.string().required().min(6),
  });
}

export = new USERVALIDATION();
