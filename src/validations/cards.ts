import Joi from 'joi';

class CARDVALIDATION {
  create = Joi.object({
    billing: {
      address: Joi.string().min(5).max(250).required(),
      city: Joi.string().min(2).max(100).required(),
      contactName: Joi.string().min(2).max(100).required(),
      country: Joi.string().min(2).max(50).required(),
      zipCode: Joi.string().required().min(2).max(10),
    },
    cardAlias: Joi.string().min(2).max(4).required(),
    cardHolderName: Joi.string().min(3).max(50).required(),
    cardNumber: Joi.string().creditCard().required(),
    cvc: Joi.string().min(3).max(3).required(),
    expireMonth: Joi.string().min(2).max(2).required(),
    expireYear: Joi.string().min(4).max(4).required(),
    lastFourDigits: Joi.string().min(4).max(4).required(),
  });
}

export = new CARDVALIDATION();
