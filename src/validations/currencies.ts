import Joi from 'joi';

class CURRENCYVALIDATION {
  list = Joi.object({
    role: Joi.array().required().items(Joi.string().valid('ROLE_ADMIN')),
  });
}

export = new CURRENCYVALIDATION();
