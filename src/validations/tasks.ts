import Joi, { ObjectSchema } from 'joi';

interface TaskType {
  title: string;
  userId?: string;
  projectId: string;
  sectiondId: string;
  description?: string;
  assignedTo?: string;
  dueDate?: Date;
  statuses?: [string];
  order?: number;
  isCompleted?: boolean;
  comments?: [string | number | Date];
  media?: [string | number | Date];
  subTasks?: [string | number];
  createdAt?: Date;
  updatedAt?: Date;
}

const taskValidation: ObjectSchema<TaskType> = Joi.object({
  title: Joi.string().required().min(2),
  projectId: Joi.string().required().min(6),
  sectionId: Joi.string().required().min(6),
  description: Joi.string().min(6),
  assignedTo: Joi.string().min(6),
  dueDate: Joi.date(),
  statuses: Joi.array(),
  order: Joi.number(),
  isCompleted: Joi.boolean(),
  comments: Joi.array(),
  media: Joi.array(),
  subTasks: Joi.array(),
});

// ! Body => req.body => params.id
const taskUpdateValidation: ObjectSchema<TaskType> = Joi.object({
  title: Joi.string().min(2),
  projectId: Joi.string().min(6),
  sectionId: Joi.string().min(6),
  description: Joi.string().min(6),
  assignedTo: Joi.string().min(6),
  dueDate: Joi.date(),
  statuses: Joi.array(),
  order: Joi.number(),
  isCompleted: Joi.boolean(),
  comments: Joi.array(),
  media: Joi.array(),
  subTasks: Joi.array(),
});

interface TaskAddCommentType {
  comment: string;
  createdAt: Date;
  userId: string;
  _id: string;
}

const taskAddCommentValidation: ObjectSchema = Joi.object({
  comment: Joi.string().required().min(3),
});

export {
  TaskType,
  TaskAddCommentType,
  taskValidation,
  taskUpdateValidation,
  taskAddCommentValidation,
};
