import express from 'express';
import USERCONTROLLER from '../controllers/users';
import USERVALIDATION from '../validations/users';
import { validate } from '../middlewares/validate';
import { authenticate } from '../middlewares/authenticate';
import { idChecker } from '../middlewares/idchecker';

const router = express.Router();

router.get('/admin/users', USERCONTROLLER.list);

router
  .route('/public/users')
  .patch(authenticate, validate(USERVALIDATION.update), USERCONTROLLER.update);

router
  .route('/public/users/:userId')
  .delete(idChecker('userId'), authenticate, USERCONTROLLER.delete);

router
  .route('/public/users/update-profile-image')
  .post(authenticate, USERCONTROLLER.profileImageUpdate);

export const UserRoute = router;
