import express from 'express';
import { validate } from '../middlewares/validate';
import { authenticate } from '../middlewares/authenticate';
import CURRENCYVALIDATION from '../validations/currencies';
import CURRENCYCONTROLLER from '../controllers/currencies';

const router = express.Router();

router
  .route('/admin/currencies')
  .post(
    authenticate,
    validate(CURRENCYVALIDATION.list),
    CURRENCYCONTROLLER.listAndCreate
  );

router
  .route('/public/currencies')
  .get(authenticate, CURRENCYCONTROLLER.exchange);

export const CurrencyRoute = router;
