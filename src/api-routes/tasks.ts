import express from 'express';
import { validate } from '../middlewares/validate';
import { authenticate } from '../middlewares/authenticate';
import { idChecker } from '../middlewares/idchecker';
import {
  taskValidation,
  taskUpdateValidation,
  taskAddCommentValidation,
} from '../validations/tasks';
import TASKCONTROLLER from '../controllers/tasks';

const router = express.Router();

router.route('/').get(authenticate, TASKCONTROLLER.list);

router
  .route('/')
  .post(authenticate, validate(taskValidation), TASKCONTROLLER.create);

router
  .route('/:id')
  .patch(
    idChecker('id'),
    authenticate,
    validate(taskUpdateValidation),
    TASKCONTROLLER.update
  );

router
  .route('/:id/add-comment')
  .post(
    idChecker('id'),
    authenticate,
    validate(taskAddCommentValidation),
    TASKCONTROLLER.addComment
  );

router
  .route('/:id/:commentId')
  .delete(idChecker('id'), authenticate, TASKCONTROLLER.deleteComment);

router
  .route('/:taskId/add-sub-task')
  .post(authenticate, validate(taskValidation), TASKCONTROLLER.addSub);

router.route('/:subTaskId').get(authenticate, TASKCONTROLLER.fetchSub);

export const TaskRoute = router;
