import express from 'express';
import { validate } from '../middlewares/validate';
import { authenticate } from '../middlewares/authenticate';
import { idChecker } from '../middlewares/idchecker';
import CARDCONTROLLER from '../controllers/cards';
import CARDVALIDATION from '../validations/cards';

const router = express.Router();

router
  .route('/public/cards/:userId')
  .get(idChecker('userId'), authenticate, CARDCONTROLLER.list);

router
  .route('/public/cards')
  .post(authenticate, validate(CARDVALIDATION.create), CARDCONTROLLER.create);

router
  .route('/public/cards/:cardId')
  .delete(idChecker('cardId'), authenticate, CARDCONTROLLER.delete);

export const CardRoute = router;
