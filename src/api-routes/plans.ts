import express from 'express';
import PLANVALIDATION from '../validations/plans';
import { validate } from '../middlewares/validate';
import { authenticate } from '../middlewares/authenticate';
import { idChecker } from '../middlewares/idchecker';
import PLANCONTROLLER from '../controllers/plans';

const router = express.Router();

router.route('/public/plans').get(PLANCONTROLLER.lists);

/*
router
  .route('/admin/plan')
  .get(
    idChecker('planId'),
    authenticate,
    validate(PLANVALIDATION.list),
    PLANCONTROLLER.list
  );
*/

router
  .route('/admin/plan')
  .post(authenticate, validate(PLANVALIDATION.create), PLANCONTROLLER.create);

router
  .route('/admin/plan/:planId')
  .patch(
    idChecker('planId'),
    authenticate,
    validate(PLANVALIDATION.create),
    PLANCONTROLLER.update
  );

router
  .route('/admin/plan/:planId')
  .delete(idChecker('id'), authenticate, PLANCONTROLLER.delete);

export const PlanRoute = router;
