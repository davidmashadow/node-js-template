import express from 'express';
import AUTHCONTROLLER from '../controllers/authentications';
import USERVALIDATION from '../validations/users';
import { validate } from '../middlewares/validate';

const router = express.Router();

router
  .route('/public/auth/register')
  .post(validate(USERVALIDATION.create), AUTHCONTROLLER.register);

router
  .route('/public/auth/register-verification')
  .post(validate(USERVALIDATION.activate), AUTHCONTROLLER.activate);

router
  .route('/public/auth/password-reset')
  .post(validate(USERVALIDATION.passwordReset), AUTHCONTROLLER.passwordReset);

router
  .route('/public/auth/password-reset-complete')
  .post(
    validate(USERVALIDATION.passwordResetComplete),
    AUTHCONTROLLER.passwordResetComplete
  );

router
  .route('/public/auth/login')
  .post(validate(USERVALIDATION.login), AUTHCONTROLLER.login);

export const AuthRoute = router;
