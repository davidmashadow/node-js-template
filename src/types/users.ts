interface UserType {
  name?: string;
  email?: string;
  phoneNumber?: string;
  password?: string;
  profileImage?: string;
  role?: [];
  isActivated?: boolean;
}

interface UserActivateType {
  key?: string;
}

interface UserLoginType {
  email?: string;
  password?: string;
}

interface UserPasswordResetType {
  email?: string;
}

interface UserPasswordResetCompleteType {
  email?: string;
}

interface UserPasswordChangeType {
  password?: string;
}

interface UserUpdateType {
  fullName?: string;
  email?: string;
  password?: string;
}

export {
  UserType,
  UserActivateType,
  UserLoginType,
  UserPasswordResetType,
  UserPasswordResetCompleteType,
  UserUpdateType,
  UserPasswordChangeType,
};
