import Mongoose from 'mongoose';

export const connectDB = async (): Promise<void> => {
  Mongoose.set('strictQuery', true);
  await Mongoose.connect(
    `${process.env.DB_SERVICE}://${process.env.DB_USERNAME}:${process.env.DB_PASSWORD}@${process.env.DB_HOST}/${process.env.DB_NAME}?retryWrites=true&w=majority`,
    {
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      useNewUrlParser: true,
      useUnifiedTopology: true,
    }
  );
};
