import { logger } from '../scripts/logger/general';
import { connectDB } from './db';

const loaders = (): void => {
  connectDB()
    .then(() =>
      logger().info(
        `Connected to ${process.env.DB_SERVICE}://${process.env.DB_USERNAME}:${process.env.DB_PASSWORD}@${process.env.DB_HOST}/${process.env.DB_NAME}?retryWrites=true&w=majority`
      )
    )
    .catch((error) => {
      logger().info(
        `${process.env.DB_SERVICE}://${process.env.DB_USERNAME}:${process.env.DB_PASSWORD}@${process.env.DB_HOST}/${process.env.DB_NAME}?retryWrites=true&w=majority`
      );
      logger().error(`DB connection is not successfull |`, error);
    });
};

export { loaders };
