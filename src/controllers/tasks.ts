import httpStatus from 'http-status';
import { Request, Response } from 'express';
import { TaskType } from '../validations/tasks';
import TASKSERVICE from '../services/mongo/tasks';

class TASKCONTROLLER {
  list = (req: Request, res: Response) => {
    if (!req?.params?.projectId) {
      res
        .status(httpStatus.BAD_REQUEST)
        .send({ error: 'Proje id degeri bulunamadi' });
    }

    TASKSERVICE.list({ projectId: req?.params?.projectId })
      .then((response: any) => res.status(httpStatus.OK).send(response))
      .catch(() =>
        res.status(httpStatus.INTERNAL_SERVER_ERROR).send({
          error: 'Task listesi cekilirken hata olustu',
        })
      );
  };

  create = (req: Request | any, res: Response) => {
    req.body.userId = req.user._doc._id;

    TASKSERVICE.create(req.body)
      .then((response: TaskType) =>
        res.status(httpStatus.CREATED).send(response)
      )
      .catch(() =>
        res
          .status(httpStatus.INTERNAL_SERVER_ERROR)
          .send({ error: 'Task olusturulurken hata meydana geldi' })
      );
  };

  update = (req: Request | any, res: Response) => {
    if (!req?.params?.id)
      res.status(httpStatus.BAD_REQUEST).send({
        message: 'ID Bilgisi eksik girilmistir',
      });

    TASKSERVICE.update(req?.params?.id, req?.body)
      .then((updatedProject: any) =>
        res.status(httpStatus.OK).send(updatedProject)
      )
      .catch(() =>
        res.status(httpStatus.INTERNAL_SERVER_ERROR).send({
          error: 'Task guncellemesi sirasinda hata olustu',
        })
      );
  };

  delete = (req: Request, res: Response) => {
    if (!req?.params?.id)
      res.status(httpStatus.BAD_REQUEST).send({
        message: 'ID Bilgisi eksik girilmistir',
      });

    TASKSERVICE.delete({ id: req?.params?.id })
      .then((deletedDoc: any) => {
        if (!deletedDoc) {
          return res
            .status(httpStatus.NOT_FOUND)
            .send({ message: 'Boyle bir Task bulunamamistir' });
        }
        return res.status(httpStatus.OK).send({ message: 'Task silinmistir' });
      })
      .catch(() =>
        res.status(httpStatus.INTERNAL_SERVER_ERROR).send({
          error: 'Task silinmesi sirasinda hata olustu',
        })
      );
  };

  addComment = (req: Request | any, res: Response) => {
    const userId = req.user._doc._id;
    const userComment = {
      ...req.body,
      createdAt: new Date(),
      userId,
    };

    TASKSERVICE.findTask({ _id: req.params.id }, false)
      .then((mainTask: TaskType | any): any => {
        if (!mainTask) {
          res.status(httpStatus.NOT_FOUND).send({
            error: 'Boyle bir yorum bulunamadi',
          });
        }

        if (mainTask?.comments) {
          mainTask?.comments.push(userComment);
          mainTask
            .save()
            .then((updatedDoc: TaskType) =>
              res.status(httpStatus.OK).send(updatedDoc)
            )
            .catch(() =>
              res.status(httpStatus.INTERNAL_SERVER_ERROR).send({
                error: 'Task yorum guncellemesi sirasinda hata olustu',
              })
            );
        }

        return res.status(httpStatus.OK).send({ message: 'Yorum eklenmistir' });
      })
      .catch(() =>
        res.status(httpStatus.INTERNAL_SERVER_ERROR).send({
          error: 'Task guncellemesi sirasinda hata olustu',
        })
      );
  };

  deleteComment = (req: Request, res: Response) => {
    TASKSERVICE.findTask({ _id: req.params.id }, false)
      .then((mainTask: TaskType | any): any => {
        if (!mainTask)
          res.status(httpStatus.NOT_FOUND).send({
            error: 'Boyle bir task bulunamadi',
          });

        if (mainTask?.comments) {
          // eslint-disable-next-line no-param-reassign
          mainTask.comments = mainTask.comments.filter(
            (comment: any) => comment._id.toString() !== req.params.commentId
          );
          mainTask
            .save()
            .then((updatedDoc: TaskType) =>
              res.status(httpStatus.OK).send(updatedDoc)
            )
            .catch(() =>
              res.status(httpStatus.INTERNAL_SERVER_ERROR).send({
                error: 'Task yorumu silinirken hata olustu',
              })
            );
        }

        return res.status(httpStatus.OK).send({ message: 'Yorum silinmistir' });
      })
      .catch(() =>
        res.status(httpStatus.INTERNAL_SERVER_ERROR).send({
          error: 'Task yorum silinmesi sirasinda hata olustu',
        })
      );
  };

  addSub = (req: Request | any, res: Response) => {
    //! MainTask cekilir
    if (!req.params.taskId) {
      res
        .status(httpStatus.BAD_REQUEST)
        .send({ message: 'Task ID bilgisi gereklidir' });
    }
    TASKSERVICE.findTask({ _id: req.params.taskId }, false)
      .then((mainTask: TaskType | any): any => {
        if (!mainTask) {
          res.status(httpStatus.NOT_FOUND).send({
            error: 'Boyle bir task bulunamadi',
          });
        }

        //! Subtask Create Edilir (Task)
        TASKSERVICE.create({ ...req.body, userId: req.user._doc._id })
          .then((subTask: TaskType) => {
            if (mainTask.subTasks) {
              //! Subtask'in Referansi MainTask uzerinde gosterilir ve Update edilir
              mainTask.subTasks.push(subTask);
              mainTask
                .save()
                .then((updatedDoc: TaskType) =>
                  //! Kullaniciya yeni dokuman gonderilir
                  res.status(httpStatus.OK).send(updatedDoc)
                )
                .catch(() =>
                  res.status(httpStatus.INTERNAL_SERVER_ERROR).send({
                    error: 'Subtask kayit sirasinda hata olustu',
                  })
                );
            }
            return res
              .status(httpStatus.OK)
              .send({ message: 'SubTask olusturulmustur' });
          })
          .catch(() =>
            res
              .status(httpStatus.INTERNAL_SERVER_ERROR)
              .send({ error: 'Subtask olusturulurken hata meydana geldi' })
          );
        return null;
      })
      .catch(() =>
        res.status(httpStatus.INTERNAL_SERVER_ERROR).send({
          error: 'Subtask guncellemesi sirasinda hata olustu',
        })
      );
  };

  fetchSub = (req: Request, res: Response) => {
    if (!req.params.subTaskId) {
      res
        .status(httpStatus.BAD_REQUEST)
        .send({ message: 'SubTask ID bilgisi gereklidir' });
    }
    TASKSERVICE.findTask({ _id: req.params.subTaskId }, true)
      .then((subTask: any) => {
        if (!subTask) {
          res
            .status(httpStatus.NOT_FOUND)
            .send({ message: 'Boyle bir subtask bulunmamaktadir' });
        }
        return res.status(httpStatus.OK).send(subTask);
      })
      .catch(() =>
        res
          .status(httpStatus.INTERNAL_SERVER_ERROR)
          .send({ message: 'Boyle bir subtask bulunamadi' })
      );
  };
}

export = new TASKCONTROLLER();
