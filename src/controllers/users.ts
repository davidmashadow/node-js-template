import httpStatus from 'http-status';
import { Request, Response, NextFunction } from 'express';
import path from 'path';
import APIERROR from '../responses/apierror';
import { APISUCCESS } from '../responses/apisuccess';
import { logger } from '../scripts/logger/general';
import { UserType } from '../types/users';
import { passwordToHash } from '../scripts/utils/password';
import USERSERVICE from '../services/mongo/users';

class USERCONTROLLER {
  list = (req: Request | any, res: Response, next: NextFunction) => {
    USERSERVICE.paginate(
      { query: { name: { $regex: req?.query?.text || '', $options: 'i' } } },
      { page: parseInt(req?.query?.page, 10) || 1 },
      { limit: parseInt(req?.query?.limit, 10) || 50 },
      { sort: String(req?.query?.sort) }
    )
      .then((usersList: UserType[]) =>
        USERSERVICE.count()
          .then((totalCount: number) => {
            logger().info(`Users Listed | TotalCount = ${totalCount}`);
            return res
              .status(httpStatus.OK)
              .send(
                APISUCCESS(`${totalCount} total users in system`, usersList)
              );
          })
          .catch((error: string) => {
            logger().error(`${error}::USERSERVICE.count()`);
            next(new APIERROR(`${error}`, httpStatus.INTERNAL_SERVER_ERROR));
          })
      )
      .catch((error: string) => {
        logger().error(`${error}::USERSERVICE.list()`);
        next(new APIERROR(`${error}`, httpStatus.INTERNAL_SERVER_ERROR));
      });
  };

  update = (req: Request | any, res: Response, next: NextFunction) => {
    const activeUserId = req?.user?._doc?._id;
    req.body.password = passwordToHash(req?.body?.password);

    logger().debug(`userUpdate::USERSERVICE.update()`);
    USERSERVICE.update(activeUserId, req?.body)
      .then((updatedUser: UserType[] | any) => {
        logger().info(`${updatedUser._id} informations updated`);
        return res
          .status(httpStatus.OK)
          .send(APISUCCESS(`User updated.`, updatedUser));
      })
      .catch((error: string) => {
        logger().error(`${error}::USERSERVICE.update()`);
        next(new APIERROR(`${error}`, httpStatus.INTERNAL_SERVER_ERROR));
      });
  };

  delete = (req: Request, res: Response, next: NextFunction) => {
    logger().debug(`userDelete::USERSERVICE.delete()`);
    USERSERVICE.delete({ id: req?.params?.id })
      .then((deletedUser: UserType) => {
        if (!deletedUser) {
          logger().warn(`notFoundAnyUser::${req?.params?.id}`);
          return next(
            new APIERROR(
              `${req?.params?.id} not match any user.`,
              httpStatus.BAD_REQUEST
            )
          );
        }

        return res
          .status(httpStatus.OK)
          .send(APISUCCESS(`User deleted in the system.`, deletedUser));
      })
      .catch((error: string) => {
        logger().error(`${error}::USERSERVICE.delete()`);
        next(new APIERROR(`${error}`, httpStatus.INTERNAL_SERVER_ERROR));
      });
  };

  profileImageUpdate = (
    req: Request | any,
    res: Response,
    next: NextFunction
  ) => {
    const userId = req?.user?._doc?._id;
    const extensionPath = path.extname(req?.files?.profileImage?.name);
    const fileName = `${userId}${extensionPath}`;
    const folderPath = path.join(__dirname, '../', 'uploads/users', fileName);

    req?.files?.profileImage?.mv(folderPath, (error: string): any => {
      if (error) {
        return next(new APIERROR(`${error}`, httpStatus.BAD_REQUEST));
      }

      logger().debug(`updateProfileImage::USERSERVICE.updateProfileImage()`);
      return USERSERVICE.update({ id: userId }, { profileImage: fileName })
        .then((updatedUser: UserType[] | any) => {
          logger().info(`${updatedUser?.email} updated profile picture`);
          return res
            .status(httpStatus.OK)
            .send(APISUCCESS(`User profile image changed.`, updatedUser));
        })
        .catch((err: string) => {
          logger().error(`${err}::USERSERVICE.updateProfileImage()`);
          next(new APIERROR(`${err}`, httpStatus.INTERNAL_SERVER_ERROR));
        });
    });
  };
}

export = new USERCONTROLLER();
