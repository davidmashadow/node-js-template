import httpStatus from 'http-status';
import { Request, Response, NextFunction } from 'express';
import USERSERVICE from '../services/mongo/users';
import CURRENCYSERVICE from '../services/mongo/currencies';
import CURRENCYREQUEST from '../services/axios/currencies';
import APIERROR from '../responses/apierror';
import { APISUCCESS } from '../responses/apisuccess';
import { logger } from '../scripts/logger/general';

class CURRENCYCONTROLLER {
  listAndCreate = (req: Request | any, res: Response, next: NextFunction) => {
    const userId = req?.user?._doc?._id;

    logger().debug(`userFind::USERSERVICE.list()`);
    return USERSERVICE.list({ _id: userId })
      .then((user: any) => {
        if (user.length > 0) {
          logger().info(`${user.length} User found with ${userId} ID`);
          if (`${req?.body?.role?.[0]}` === `${user?.[0]?.role?.[0]}`) {
            logger().debug(`currenciesDelete::CURRENCYSERVICE.deleteAll()`);
            return CURRENCYSERVICE.deleteAll()
              .then((response: any) => {
                logger().info(
                  `${response?.deletedCount} rows deleted currencies collection `
                );
                logger().debug(`currenciesList::CURRENCYREQUEST.list()`);
                return CURRENCYREQUEST.list()
                  .then((data) => {
                    logger().debug(
                      `currenciesCreate::CURRENCYSERVICE.create()`
                    );
                    return CURRENCYSERVICE.create(data)
                      .then((currencies: any) => {
                        logger().info(
                          `${
                            data.length + 1
                          } rows created in the currencies collection.`
                        );
                        return res
                          .status(httpStatus.OK)
                          .send(
                            APISUCCESS(`Currencies updated in the system`, data)
                          );
                      })
                      .catch((error: string) => {
                        logger().error(`${error}::CURRENCYSERVICE.create()`);
                        next(
                          new APIERROR(
                            `${error}`,
                            httpStatus.INTERNAL_SERVER_ERROR
                          )
                        );
                      });
                  })
                  .catch((error: string) => {
                    logger().error(`${error}::CURRENCYREQUEST.list()`);
                    next(
                      new APIERROR(`${error}`, httpStatus.INTERNAL_SERVER_ERROR)
                    );
                  });
              })
              .catch((error: string) => {
                logger().error(`${error}::CURRENCYREQUEST.deleteAll()`);
                next(
                  new APIERROR(`${error}`, httpStatus.INTERNAL_SERVER_ERROR)
                );
              });
          }
          logger().error(`${userId} ID don't have a ROLE_ADMIN in system`);
          return next(
            new APIERROR(
              `${user?.[0]?._id} ID don't have a ROLE_ADMIN in system`,
              httpStatus.BAD_REQUEST
            )
          );
        }
        logger().error(`${userId} is not matching any user`);
        return next(
          new APIERROR(
            `${user?.[0]?._id} ID is not matching any User in system`,
            httpStatus.BAD_REQUEST
          )
        );
      })
      .catch((error: string) => {
        logger().error(`${error}::USERSERVICE.list()`);
        next(new APIERROR(`${error}`, httpStatus.INTERNAL_SERVER_ERROR));
      });
  };

  exchange = (req: Request | any, res: Response, next: NextFunction) => {
    const from = `${req?.query?.from}`;
    const quantity = parseFloat(req?.query?.quantity);
    const to = `${req?.query?.to}`;
    let result = null;

    if (from === to) {
      return res
        .status(httpStatus.OK)
        .send(APISUCCESS(`Currencies listed`, quantity));
    }

    return CURRENCYSERVICE.find({ unit: [from, to] })
      .then((currencies: any) => {
        logger().info(
          `${currencies?.[0]?.unit} and ${currencies?.[1]?.unit} Listed`
        );
        const firstValue = parseFloat(currencies?.[0].sell.replace(',', '.'));
        const secondValue = parseFloat(currencies?.[1].sell.replace(',', '.'));

        if (from === currencies?.[0].unit) {
          result = (firstValue * quantity) / secondValue;
        } else {
          result = (secondValue * quantity) / firstValue;
        }

        result = parseFloat(result.toFixed(2));

        return res
          .status(httpStatus.OK)
          .send(APISUCCESS(`Currencies listed`, result));
      })
      .catch((error: string) => {
        logger().error(`${error}::CARDSERVICE.list()`);
        next(new APIERROR(`${error}`, httpStatus.INTERNAL_SERVER_ERROR));
      });
  };
}

export = new CURRENCYCONTROLLER();
