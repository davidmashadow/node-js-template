import httpStatus from 'http-status';
import { Request, Response, NextFunction } from 'express';
import APIERROR from '../responses/apierror';
import { APISUCCESS } from '../responses/apisuccess';
import { logger } from '../scripts/logger/general';
import CARDSERVICE from '../services/mongo/cards';

class CARDCONTROLLER {
  list = (req: Request | any, res: Response, next: NextFunction) => {
    if (`${req?.user?._doc?._id}` === `${req?.params?.userId}`) {
      return CARDSERVICE.list({ userId: req?.params?.userId })
        .then((cardsList: any) => {
          logger().info(`${req?.user?._doc?._id} | User cards listed`);
          return res
            .status(httpStatus.OK)
            .send(
              APISUCCESS(
                `${req?.user?._doc?._id} | User cards listed`,
                cardsList
              )
            );
        })
        .catch((error: string) => {
          logger().error(`${error}::CARDSERVICE.list()`);
          next(new APIERROR(`${error}`, httpStatus.INTERNAL_SERVER_ERROR));
        });
    }
    logger().error(
      `${req?.params?.userId} is not matching ${req?.user?._doc?._id}`
    );
    return next(
      new APIERROR(
        `${req?.params?.userId}(sendingId) is not matching ${req?.user?._doc?._id}(userId)`,
        httpStatus.BAD_REQUEST
      )
    );
  };

  create = (req: Request | any, res: Response, next: NextFunction) => {
    req.body.userId = req.user._doc._id;

    CARDSERVICE.create(req?.body)
      .then((userCards: any) => {
        logger().info(`${req?.body?.userId} has created card in the system`);
        return res
          .status(httpStatus.CREATED)
          .send(
            APISUCCESS(
              `${req?.body?.userId} has created card in the system`,
              userCards
            )
          );
      })
      .catch((error: string) => {
        logger().error(`${error}::CARDSERVICE.create()`);
        next(new APIERROR(`${error}`, httpStatus.INTERNAL_SERVER_ERROR));
      });
  };

  delete = (req: Request | any, res: Response, next: NextFunction) => {
    CARDSERVICE.list({ userId: req?.user?._doc?._id })
      .then((cardsList: any) => {
        if (cardsList?.length > 0) {
          if (`${cardsList?.[0]?._id}` === `${req?.params?.cardId}`) {
            return CARDSERVICE.delete({ id: `${req.params.cardId}` })
              .then((deletedCard: any) => {
                logger().info(`${deletedCard?._id} | card deleted`);
                return res
                  .status(httpStatus.OK)
                  .send(
                    APISUCCESS(
                      `${req?.params?.cardId} | Card deleted`,
                      deletedCard
                    )
                  );
              })
              .catch((error: string) => {
                logger().error(`${error}::CARDSERVICE.delete()`);
                next(
                  new APIERROR(`${error}`, httpStatus.INTERNAL_SERVER_ERROR)
                );
              });
          }
          // USER CARD ID NOT MATCHING SENDING CARD ID
          logger().error(
            `${req?.params?.cardId} is not matching ${cardsList?.[0]?._id}`
          );
          return next(
            new APIERROR(
              `${req?.params?.cardId}(sendingcardId) is not matching ${cardsList?.[0]?._id}(userCardId)`,
              httpStatus.BAD_REQUEST
            )
          );
        }
        // USER DON'T HAVE ANY CARDS
        logger().info(`${req?.user?._doc?._id} has ${cardsList.length} card.`);
        return res
          .status(httpStatus.OK)
          .send(
            APISUCCESS(
              `${req?.user?._doc?._id} don't have any card.`,
              cardsList
            )
          );
      })
      .catch((error: string) => {
        logger().error(`${error}::CARDSERVICE.list()`);
        next(new APIERROR(`${error}`, httpStatus.INTERNAL_SERVER_ERROR));
      });
  };
}

export = new CARDCONTROLLER();
