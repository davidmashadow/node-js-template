import httpStatus from 'http-status';
import { Request, Response, NextFunction } from 'express';
import { v4 as uuidv4 } from 'uuid';
import APIERROR from '../responses/apierror';
import { APISUCCESS } from '../responses/apisuccess';
import { logger } from '../scripts/logger/general';
import { UserType, UserLoginType } from '../types/users';
import { passwordToHash } from '../scripts/utils/password';
import { eventEmitter } from '../scripts/events/eventemitter';
import USERSERVICE from '../services/mongo/users';
import {
  generateAccessToken,
  generateRefreshToken,
} from '../scripts/utils/token';

class AUTHCONTROLLER {
  register = (req: Request, res: Response, next: NextFunction) => {
    req.body.password = passwordToHash(req?.body?.password);

    logger().debug(`userlist::USERSERVICE.list()`);
    USERSERVICE.list({ email: req?.body?.email })
      .then((response: UserType[]) => {
        // MAIL EXIST
        if (response?.length > 0) {
          logger().warn(`repeatingMail::${response[0]?.email}`);
          return next(
            new APIERROR(
              `${response[0]?.email} using. Try different mail`,
              httpStatus.BAD_REQUEST
            )
          );
        }

        logger().debug(`userCreate::USERSERVICE.create()`);
        return USERSERVICE.create(req.body)
          .then((createdUser: UserType[] | any) => {
            // USER CREATED
            logger().info(`${createdUser?.email} created in the system.`);

            const userId = createdUser?._id;
            const code = uuidv4().split('-').join('');

            logger().debug(`userUpdate::USERSERVICE.update()`);
            return USERSERVICE.update({ id: userId }, { activationCode: code })
              .then((updatedUser: UserType[] | any) => {
                // USER UPDATED
                logger().info(
                  `${updatedUser?.email} | ${updatedUser?.activationCode} created in the system.`
                );
                // SEND ACTIVATION CODE
                logger().debug(`eventEmitter::sendActivationEmail()`);
                eventEmitter.emit('sendEmail', {
                  to: `${updatedUser?.email}`,
                  subject: 'Mail aktivasyon işlemi',
                  html: `<div style="text-align: center; font-size: 24px; font-family: Arial, sans-serif"><span style="font-size: 120px">📬</span><br /><br /><b>${updatedUser?.name}</b> aramıza hoşgeldin. <br /> <a href="${process.env.BASE_URL}" target="_blank">${process.env.MAIN_NAME}</a><br /><br /> ${process.env.MAIN_NAME} ailesi olarak sizleri aramızda görmekten büyük bir memnuniyet duyuyoruz. Üyeliğinizi tamamlamasına sadece son bir adım kaldı. Aşağıdaki linke tıklayarak üyeliğini aktifleştirebilirsin. <br /><br/ > <div style="width: 35%; height: 30px; margin: 0 auto; padding: 10px 0 5px 0; border-radius: 15px; text-align: center; font-weight: bold; cursor: pointer; background: #FFFF00"><a href="${process.env.BASE_URL}/users/register-verification?key=${updatedUser?.activationCode}">Üyeliğini aktifleştir</a></div><br /><br />Herhangi bir sıkıntı veya sorun yaşamanız halinde bizimle <a href="mailto:${process.env.MAIN_MAIL}"></a>${process.env.MAIN_MAIL} üzerinden iletişime geçebilirsiniz. <br /><br />SAYGILARIMIZLA<br /><b>${process.env.MAIN_NAME}</b></div>`,
                });

                return res
                  .status(httpStatus.CREATED)
                  .send(
                    APISUCCESS(
                      `Registration is successfull. Activation mail sending to the ${updatedUser?.email}. Please activate your account`,
                      updatedUser
                    )
                  );
              })
              .catch((error: string) => {
                logger().error(`${error}::USERSERVICE.update()`);
                next(
                  new APIERROR(`${error}`, httpStatus.INTERNAL_SERVER_ERROR)
                );
              });
          })
          .catch((error: string) => {
            logger().error(`${error}::USERSERVICE.create()`);
            next(new APIERROR(`${error}`, httpStatus.INTERNAL_SERVER_ERROR));
          });
      })
      .catch((error: string) => {
        logger().debug(`userCreate::USERSERVICE.list()`);
        next(new APIERROR(`${error}`, httpStatus.INTERNAL_SERVER_ERROR));
      });
  };

  activate = (req: Request, res: Response, next: NextFunction) => {
    logger().debug(`userlist::USERSERVICE.list()`);

    USERSERVICE.list({ activationCode: req?.body?.key })
      .then((response: UserType[] | any) => {
        // THERE IS NO ANY USER
        if (response.length === 0) {
          return next(
            new APIERROR(
              `${req?.body?.key} key is invalid.`,
              httpStatus.BAD_REQUEST
            )
          );
        }

        // UPDATE USERACTIVATION
        logger().debug(`userlist::USERSERVICE.update()`);
        const userId = response?.[0]?._id;

        return USERSERVICE.update(
          { id: userId },
          {
            activationCode: null,
            isActivated: true,
          }
        )
          .then((updatedUser: UserType[] | any) => {
            logger().info(`${updatedUser?.email} has been activated.`);
            return res
              .status(httpStatus.OK)
              .send(APISUCCESS(`Activation is successfull.`, {}));
          })
          .catch((error: string) => {
            logger().error(`${error}::USERSERVICE.update()`);
            next(new APIERROR(`${error}`, httpStatus.INTERNAL_SERVER_ERROR));
          });
      })
      .catch((error: string) => {
        logger().error(`${error}::USERSERVICE.list()`);
        next(new APIERROR(`${error}`, httpStatus.INTERNAL_SERVER_ERROR));
      });
  };

  passwordReset = (req: Request, res: Response, next: NextFunction) => {
    logger().debug(`userlist::USERSERVICE.list()`);

    const userMail = req?.body?.email;
    const code = uuidv4().split('-').join('');

    USERSERVICE.list({ email: userMail })
      .then((response: UserType[] | any) => {
        // MAIL EXIST
        if (response?.length > 0) {
          logger().warn(`repeatingMail::${response[0]?.email}`);
          return next(
            new APIERROR(
              `${response[0]?.email} using. Try different mail`,
              httpStatus.BAD_REQUEST
            )
          );
        }

        logger().debug(`userResetPassword::USERSERVICE.updateWhere()`);
        return USERSERVICE.updateWhere(
          { mail: userMail },
          { activationCode: code }
        )
          .then((updatedUser: UserType[] | any) => {
            // USER UPDATED
            logger().info(
              `Password reset request for ${updatedUser?.email} | ${updatedUser?.activationCode}.`
            );

            // SEND ACTIVATION CODE
            logger().debug(`eventEmitter::sendActivationEmail()`);
            eventEmitter.emit('sendEmail', {
              to: `${updatedUser?.email}`,
              subject: 'Şifre sıfırlama işlemi',
              html: `<div style="text-align: center; font-size: 24px; font-family: Arial, sans-serif"><span style="font-size: 120px">🔐</span><br /><br />Merhaba <b>${updatedUser?.name}</b>. <br /> <a href="${process.env.BASE_URL}" target="_blank">${process.env.MAIN_NAME}</a><br /><br /> Şifre sıfırlamak için tarafımıza bir istekte bulundun. Eğer bu ıstekte bulunan sen değilsen lütfen bu e-postayı dikkate alma. <br /><br/ > <div style="width: 35%; height: 30px; margin: 0 auto; padding: 10px 0 5px 0; border-radius: 15px; text-align: center; font-weight: bold; cursor: pointer; background: #FFFF00"><a href="${process.env.BASE_URL}/users/reset-password?key=${updatedUser?.activationCode}">Şifreni değiştir</a></div><br /><br />Herhangi bir sıkıntı veya sorun yaşamanız halinde bizimle <a href="mailto:${process.env.MAIN_MAIL}"></a>${process.env.MAIN_MAIL} üzerinden iletişime geçebilirsiniz. <br /><br />SAYGILARIMIZLA<br /><b>${process.env.MAIN_NAME}</b></div>`,
            });

            return res
              .status(httpStatus.OK)
              .send(
                APISUCCESS(
                  `Password reset requested is successfull. Request mail sending to the ${updatedUser?.email}. Please click the link and apply your new password.`,
                  updatedUser
                )
              );
          })
          .catch((error: string) => {
            logger().error(`${error}::USERSERVICE.updateWhere()`);
            next(new APIERROR(`${error}`, httpStatus.INTERNAL_SERVER_ERROR));
          });
      })
      .catch((error: string) => {
        logger().error(`${error}::USERSERVICE.userResetPassword()`);
        next(new APIERROR(`${error}`, httpStatus.INTERNAL_SERVER_ERROR));
      });
  };

  passwordResetComplete = (req: Request, res: Response, next: NextFunction) => {
    logger().debug(`userlist::USERSERVICE.list()`);

    USERSERVICE.list({ email: req?.body?.email })
      .then((response: UserType[] | any) => {
        // MAIL NOT EXIST
        if (response?.length === 0) {
          logger().warn(`mailNotInTheSystem::${req?.body?.email}`);
          return next(
            new APIERROR(
              `${req?.body?.email} is not accessing any user`,
              httpStatus.BAD_REQUEST
            )
          );
        }

        const userPassword = passwordToHash(req?.body?.newPassword);
        const userId = response?.[0]?._id;
        const userKey = response?.[0]?.activationCode;

        if (userKey === req?.body?.key) {
          logger().debug(`userlist::USERSERVICE.update()`);

          return USERSERVICE.update(userId, {
            activationCode: null,
            password: userPassword,
          })
            .then((updatedUser: UserType[] | any) => {
              logger().info(
                `${updatedUser?.email} | ${updatedUser?.password} updated in the system.`
              );
              return res
                .status(httpStatus.OK)
                .send(
                  APISUCCESS(
                    'Your password changed. Please log in with new Password',
                    {}
                  )
                );
            })
            .catch((error: string) => {
              logger().error(`${error}::USERSERVICE.update()`);
              next(new APIERROR(`${error}`, httpStatus.INTERNAL_SERVER_ERROR));
            });
        }

        return next(
          new APIERROR(`${req?.body?.key} is not valid`, httpStatus.BAD_REQUEST)
        );
      })
      .catch((error: string) => {
        logger().debug(`userCreate::USERSERVICE.list()`);
        next(new APIERROR(`${error}`, httpStatus.INTERNAL_SERVER_ERROR));
      });
  };

  login = (req: Request, res: Response, next: NextFunction) => {
    req.body.password = passwordToHash(req?.body?.password);

    logger().debug(`userLogin::USERSERVICE.findOne()`);
    USERSERVICE.findOne(req.body)
      .then((user: UserLoginType | any) => {
        if (!user) {
          logger().warn(`notFoundAnyUser::${req?.body?.email}`);
          return next(
            new APIERROR(
              `E-mail or password is wrong. Please try again`,
              httpStatus.BAD_REQUEST
            )
          );
        }
        // CHECK ACTIVATION
        if (!user?.isActivated) {
          logger().warn(`emailIsNotActivated::${req?.body?.email}`);
          return next(
            new APIERROR(
              `${req?.body?.email} account is not activated. Please activate your email address.`,
              httpStatus.FORBIDDEN
            )
          );
        }
        // JWT ACCESS VE REFRESH TOKEN EKLE
        const userInfo = {
          email: user.email,
          password: user.password,
          accessToken: generateAccessToken(user),
          refreshToken: generateRefreshToken(user),
        };

        // ISTENMEYEN ALANLARI KALDIR VERIYI DON
        delete userInfo.password;
        return res
          .status(httpStatus.OK)
          .send(APISUCCESS(`User token generated.`, userInfo));
      })
      .catch((error: string) => {
        logger().error(`${error}::USERSERVICE.delete()`);
        next(new APIERROR(`${error}`, httpStatus.INTERNAL_SERVER_ERROR));
      });
  };
}

export = new AUTHCONTROLLER();
