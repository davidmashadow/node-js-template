import httpStatus from 'http-status';
import { Request, Response, NextFunction } from 'express';
import USERSERVICE from '../services/mongo/users';
import PLANSERVICE from '../services/mongo/plans';
import { APISUCCESS } from '../responses/apisuccess';
import { logger } from '../scripts/logger/general';
import APIERROR from '../responses/apierror';

class SECTIONCONTROLLER {
  /*
  list = (req: Request, res: Response, next: NextFunction) => {
    const planId = req?.params?.planId;

    PLANSERVICE.list({ planId })
      .then((plan: any) => {
        logger().info(`planService.list`);
        return res
          .status(httpStatus.OK)
          .send(APISUCCESS(`planService.list`, {}));
      })
      .catch((error: string) => {
        logger().error(`${error}::PLANSERVICE.list()`);
        return next(new APIERROR(`${error}`, httpStatus.INTERNAL_SERVER_ERROR));
      });
  };
  */
  lists = (req: Request, res: Response, next: NextFunction) => {
    logger().debug(`userFind::PLANSERVICE.lists()`);
    PLANSERVICE.list({})
      .then((plans: any) => {
        logger().info(`${plans.length} plans listed`);
        return res
          .status(httpStatus.OK)
          .send(APISUCCESS(`planService.lists`, plans));
      })
      .catch((error: string) => {
        logger().error(`${error}::PLANSERVICE.lists()`);
        return next(new APIERROR(`${error}`, httpStatus.INTERNAL_SERVER_ERROR));
      });
  };

  create = (req: Request | any, res: Response, next: NextFunction) => {
    const userId = req?.user?._doc?._id;

    logger().debug(`userFind::USERSERVICE.list()`);
    return USERSERVICE.list({ _id: userId })
      .then((user: any) => {
        if (user.length > 0) {
          logger().info(`${user.length} User found with ${userId} ID`);

          if (req?.body?.role?.[0] === user?.[0]?.role?.[0]) {
            return PLANSERVICE.create(req.body)
              .then((data: any) => {
                logger().info(`New plan | ${data?.code} | created`);
                return res
                  .status(httpStatus.OK)
                  .send(
                    APISUCCESS(`${data?.code} plan has been created`, data)
                  );
              })
              .catch((error: string) => {
                logger().error(`${error}::PLANSERVICE.create()`);
                next(
                  new APIERROR(`${error}`, httpStatus.INTERNAL_SERVER_ERROR)
                );
              });
          }
          logger().error(`${userId} ID don't have a ROLE_ADMIN in system`);
          return next(
            new APIERROR(
              `${user?.[0]?._id} ID don't have a ROLE_ADMIN in system`,
              httpStatus.BAD_REQUEST
            )
          );
        }
        logger().error(`${userId} is not matching any user`);
        return next(
          new APIERROR(
            `${user?.[0]?._id} ID is not matching any User in system`,
            httpStatus.BAD_REQUEST
          )
        );
      })
      .catch((error: string) => {
        logger().error(`${error}::USERSERVICE.list()`);
        next(new APIERROR(`${error}`, httpStatus.INTERNAL_SERVER_ERROR));
      });
  };

  update = (req: Request | any, res: Response, next: NextFunction) => {
    const userId = req?.user?._doc?._id;
    const planId = req?.params?.planId;

    logger().debug(`userFind::USERSERVICE.list()`);
    return USERSERVICE.list({ _id: userId })
      .then((user: any) => {
        if (user.length > 0) {
          logger().info(`${user.length} User found with ${userId} ID`);

          if (req?.body?.role?.[0] === user?.[0]?.role?.[0]) {
            return PLANSERVICE.update({ id: planId }, req.body)
              .then((data: any) => {
                logger().info(`New plan | ${data?.code} | updated`);
                return res
                  .status(httpStatus.OK)
                  .send(
                    APISUCCESS(`${data?.code} plan has been updated`, data)
                  );
              })
              .catch((error: string) => {
                logger().error(`${error}::PLANSERVICE.create()`);
                next(
                  new APIERROR(`${error}`, httpStatus.INTERNAL_SERVER_ERROR)
                );
              });
          }
          logger().error(`${userId} ID don't have a ROLE_ADMIN in system`);
          return next(
            new APIERROR(
              `${user?.[0]?._id} ID don't have a ROLE_ADMIN in system`,
              httpStatus.BAD_REQUEST
            )
          );
        }
        logger().error(`${userId} is not matching any user`);
        return next(
          new APIERROR(
            `${user?.[0]?._id} ID is not matching any User in system`,
            httpStatus.BAD_REQUEST
          )
        );
      })
      .catch((error: string) => {
        logger().error(`${error}::USERSERVICE.list()`);
        next(new APIERROR(`${error}`, httpStatus.INTERNAL_SERVER_ERROR));
      });
  };

  delete = (req: Request | any, res: Response, next: NextFunction) => {
    PLANSERVICE.delete({ id: req?.params?.id })
      .then((deletedDoc: any) => {
        if (!deletedDoc)
          res.status(httpStatus.NOT_FOUND).send({
            message: 'Silmek istediginiz section sistemde mevcut degildir',
          });

        return res
          .status(httpStatus.OK)
          .send({ message: 'Section silinmistir' });
      })
      .catch(() =>
        res.status(httpStatus.INTERNAL_SERVER_ERROR).send({
          error: 'Section silinirken bir hata olustu',
        })
      );
  };
}

export = new SECTIONCONTROLLER();
