import { BASESERVICE } from './baseservice';
import { PLANRULE } from '../../models/plans';

class SECTIONSERVICE extends BASESERVICE {
  public PLANRULE = PLANRULE;

  constructor() {
    super(PLANRULE);
  }
}

export = new SECTIONSERVICE();
