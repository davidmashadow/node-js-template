import { BASESERVICE } from './baseservice';
import { CURRENCYRULE } from '../../models/currencies';

class CURRENCYSERVICE extends BASESERVICE {
  public CURRENCYRULE = CURRENCYRULE;

  constructor() {
    super(CURRENCYRULE);
  }

  create(data?: any): any {
    if (data.length > 0) {
      data.forEach((item: any) => {
        this.BaseModel(item).save();
      });
    }
    return this.BaseModel({
      unit: 'TRY',
      buy: '1',
      sell: '1',
      changeRate: '0,00%',
      updateTime: `${new Date().getHours()}:00`,
    }).save();
  }
}

export = new CURRENCYSERVICE();
