import { BASESERVICE } from './baseservice';
import { CARDRULE } from '../../models/cards';

class CARDSERVICE extends BASESERVICE {
  public CARDRULE = CARDRULE;

  constructor() {
    super(CARDRULE);
  }

  list(where: { userId: string }): any {
    return this.CARDRULE.find(where).populate({
      path: 'userId',
      select: 'name email',
    });
  }
}

export = new CARDSERVICE();
