class BASESERVICE {
  constructor(public BaseModel: any) {}

  list(where?: any): any {
    return this.BaseModel.find(where || {});
  }

  count(): any {
    return this.BaseModel.find({}).count();
  }

  paginate(
    { query }: { query: any },
    { page }: { page: number },
    { limit }: { limit: number },
    { sort }: { sort: any }
  ): any {
    // CHECK SOME SORT RULES
    if (typeof sort === 'undefined') {
      throw new Error(
        'Please define a available type and property. email,desc || name,asc'
      );
    }

    const editedSort = sort.split(',');
    if (editedSort.length < 2) {
      throw new Error(
        'Please define a available type and property. email,desc || name,asc'
      );
    }

    const sortName = editedSort[0].trim();
    let sortType = editedSort[1].trim();
    switch (sortType) {
      case 'asc':
        sortType = 1;
        break;
      case 'desc':
        sortType = -1;
        break;
      default:
        sortType = 0;
    }

    if (sortType === 0) {
      throw new Error(`Please define a available property. asc || desc`);
    }

    const sortData = { [sortName]: sortType };
    return this.BaseModel.find(query || {})
      .skip((page - 1) * limit)
      .limit(limit)
      .sort(sortData);
  }

  find(where?: any): any {
    return this.BaseModel.find(where);
  }

  findOne(where?: any): any {
    return this.BaseModel.findOne(where);
  }

  findById({ id }: { id: string }): any {
    return this.BaseModel.findById(id);
  }

  create(data?: any): any {
    return new this.BaseModel(data).save();
  }

  update({ id }: { id: string }, data?: any): any {
    return this.BaseModel.findByIdAndUpdate(id, data, { new: true });
  }

  updateWhere(where?: any, data?: any): any {
    return this.BaseModel.findOneAndUpdate(where, data, { new: true });
  }

  delete({ id }: { id: string }): any {
    return this.BaseModel.findByIdAndDelete(id);
  }

  deleteAll(): any {
    return this.BaseModel.deleteMany({});
  }
}

export { BASESERVICE };
