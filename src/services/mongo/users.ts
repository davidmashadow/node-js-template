import { BASESERVICE } from './baseservice';
import { USERRULE } from '../../models/users';

class USERSERVICE extends BASESERVICE {
  public USERRULE = USERRULE;

  constructor() {
    super(USERRULE);
  }
}

export = new USERSERVICE();
