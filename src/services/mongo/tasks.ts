import { BASESERVICE } from './baseservice';
import { TASKRULE } from '../../models/tasks';

class TASKSERVICE extends BASESERVICE {
  public TASKRULE = TASKRULE;

  constructor() {
    super(TASKRULE);
  }

  list(where?: any): any {
    return this.TASKRULE.find(where || {}).populate({
      path: 'userId',
      select: 'fullName email profileImage',
    });
  }

  findTask(where: any, expand: boolean) {
    if (!expand) {
      return this.TASKRULE.findOne(where);
    }
    return this.TASKRULE.findOne(where).populate([
      {
        path: 'userId',
        select: 'fullName email profileImage',
      },
      {
        path: 'comments',
        populate: {
          path: 'userId',
          select: 'fullName email profileImage',
        },
      },
      {
        path: 'subTasks',
        select: 'title description isCompleted assignedTo dueDate subTasks',
      },
    ]);
  }
}

export = new TASKSERVICE();
