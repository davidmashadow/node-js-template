import axios from 'axios';
import { JSDOM } from 'jsdom';
import httpStatus from 'http-status';
import { logger } from '../../scripts/logger/general';
import APIERROR from '../../responses/apierror';
import { outerHTML } from '../../scripts/utils/domconverter';

class CURRENCYREQUEST {
  public currencyData: any = {};

  async list() {
    return axios
      .get('https://bigpara.hurriyet.com.tr/doviz/', {
        headers: {
          Accept: '*/*',
          verificationtoken:
            'ZFr32Rx4ZNiFSqEsBO4LkaQl925igswCeYT3__kCjrO8eD0_J3gGAGIZi3Rsc6SzuSpgtLmMK-unA1YU4qVybB-bD-c1,SpjMT0Oi3cyCJNWb6GTlAZeUoJYDuyNOYHG5nnpFHDRgEQOFOuDCwGwFzpyoC_2V6bT0jBqUC0H1cws1UAFVCO7bzis1',
          'User-Agent':
            'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/102.0.5005.124 Safari/537.36 Edg/102.0.1245.44',
          'X-Requested-With': 'XMLHttpRequest',
        },
      })
      .then((response: any) => {
        const DOM = new JSDOM(response?.data.toString()).window.document;

        const mainElement = DOM.querySelector('.srbstPysDvz');
        const divElement = mainElement?.querySelector('.tBody');
        const ulTags = divElement?.querySelectorAll('ul');

        const unit: string[] = [];
        const buy: string[] = [];
        const sell: string[] = [];
        const rate: string[] = [];
        const time: string[] = [];

        ulTags?.forEach((tags) => {
          // CURRENCY UNITS
          const imageTag = tags.querySelector('img');
          const srcTag = imageTag?.src; // imageTag?.getAttribute('src');
          const currencyUnit = srcTag?.slice(48, 51);
          unit.push(`${currencyUnit}`);
          // CURRENCY BUY
          const currencyBuy = tags.querySelector('li:nth-child(3)');
          buy.push(`${currencyBuy?.textContent}`);
          // CURRENCY SELL
          const currencySell = tags.querySelector('li:nth-child(4)');
          sell.push(`${currencySell?.textContent}`);
          // CURRENCY CHANGE RATE
          const currencyRate = tags.querySelector('li:nth-child(5)');
          rate.push(`${currencyRate?.textContent}`);
          // CURRENCY UPDATE TIME
          const currencyTime = tags.querySelector('li:nth-child(6)');
          time.push(`${currencyTime?.textContent}`);
        });

        if (unit.length === 12) {
          this.currencyData = unit.map((item, index) => ({
            unit: unit[index],
            buy: buy[index],
            sell: sell[index],
            changeRate: rate[index],
            updateTime: time[index],
          }));
        }
        return this.currencyData;
      })
      .catch((error: string) => {
        logger().error(`${error}::CURRENCYREQUEST.list()`);
        return new APIERROR(`${error}`, httpStatus.INTERNAL_SERVER_ERROR);
      });
  }
}

export = new CURRENCYREQUEST();
