import dotenv from 'dotenv-safe';
import path from 'path';

export const server = (): void => {
  dotenv.config({
    path: `${path.resolve(__dirname, '../../')}/.env`,
    example: `${path.resolve(__dirname, '../../')}/.env.prod`,
    allowEmptyValues: true,
  });
};
